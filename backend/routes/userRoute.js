'use strict';
module.exports = function(app) {
  var userController = require('../controller/userController');

  // category Routes
  app.route('/user')
  .post(userController.createUser);

  app.route('/authenticate')
  .post(userController.authenticate);

  app.route('/refreshToken')
  .post(userController.refreshToken);

};
