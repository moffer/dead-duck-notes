'use strict';
var router =  require('express').Router({mergeParams: true});
var categoryController = require('../controller/categoryController');

// category Routes
router.route('/')
  .get(categoryController.list_all_categories)
  .put(categoryController.createACategory);

router.route('/categoryWithNotes')
  .get(categoryController.list_all_categories_with_notes);
router.route('/:categoryId')
  // TODO change to post
  .put(categoryController.updateACategory)
  .get(categoryController.getCategory)
  .delete(categoryController.deleteCategory);

module.exports = {
  baseRoute: '/category',
  router: router
}