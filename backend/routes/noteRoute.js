'use strict';
var router =  require('express').Router({mergeParams: true});

var noteController = require('../controller/noteController');

  // category Routes
  router.route('/')
  // TODO change route to /note get
  .get(noteController.getAllNotes)
  .put(noteController.createOrUpdateNote);

  router.route('/:noteId')
  .get(noteController.getNote)
  .delete(noteController.deleteNote);
  
  router.route('/:noteId/history')
  .get(noteController.getHistoryOfNote)

  router.route('/:noteId/history/:historyId')
  .delete(noteController.deleteHistoryEntryOfNote);
  router.route('/:noteId/history/:historyId/restore')
  .put(noteController.restoreHistoryEntryOfNote);

module.exports = {
  baseRoute: '/:categoryId/note',
  router: router
}