'use strict';
var router =  require('express').Router({mergeParams: true});

var noteController = require('../controller/noteController');

router.route('/')
.get(noteController.getTrash)
.delete(noteController.deleteTrash);

router.route('/:noteId').delete(noteController.deleteNoteInTrash);


module.exports = {
    baseRoute: '/trash',
    router: router
  }