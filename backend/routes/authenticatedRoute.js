'use strict';
var authorization = require('../controller/Authorization');
var router =  require('express').Router();


router.options("/*", (req, res, next) => {res.sendStatus(200);}).use("/", authorization.isLoggedIn);

module.exports = {
    baseRoute: '/authenticated',
    router: router
  }