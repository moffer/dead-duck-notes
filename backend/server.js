#!/usr/bin/env node

// Start as linux service
var express = require('express');
var app = express();

var config = require('./config');
// var MongoClient = require('mongodb').MongoClient
// , assert = require('assert');

var mongoose = require('mongoose'), bodyParser = require('body-parser');
var Category = require('./models/categoryModel');
var Note = require('./models/noteModel');
var User = require('./models/userModel');

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect(config.database); 

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req,res,next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('content-type', 'text/javascript');
    next();
});

var authenticatedRoutes = require('./routes/authenticatedRoute');
app.use(authenticatedRoutes.baseRoute, authenticatedRoutes.router);

var categoryRoutes = require('./routes/categoryRoute'); //importing route
authenticatedRoutes.router.use(categoryRoutes.baseRoute, categoryRoutes.router);
var noteRoutes = require('./routes/noteRoute');
categoryRoutes.router.use(noteRoutes.baseRoute, noteRoutes.router);

var trashRoutes = require('./routes/trashRoute');
authenticatedRoutes.router.use(trashRoutes.baseRoute, trashRoutes.router);

var userRoutes = require('./routes/userRoute');
userRoutes(app);

var server = app.listen(8081, function () {
  var host = server.address().address
  var port = server.address().port
  console.log("Dead-Duck-Note backend at http://%s:%s", host, port)
})