FROM node:8.11

WORKDIR /usr/src/app

# ARG NODE_ENV
# ENV NODE_ENV $NODE_ENV

COPY package.json /usr/src/app/
RUN npm install

VOLUME ["/usr/src/app/src"]

EXPOSE 8081
CMD [ "npm", "run", "dev-docker"]