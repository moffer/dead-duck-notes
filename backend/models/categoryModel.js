'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Category = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the category'
  },
  color: {
    type: String
  },
  userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Category', Category, 'category');