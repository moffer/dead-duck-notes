'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var mongooseHistory = require('mongoose-history');

var Note = new Schema({
  name: { type: String, required: 'Kindly enter the name of the Note'
  },
  type: String,
  categoryId: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'  },
  markdown: Boolean,
  content: {
    text: String,
    noteItems: [{
      name: String,
      checked: Boolean
    }]
  }
});

Note.plugin(mongooseHistory)
module.exports = mongoose.model('Note', Note, 'note');