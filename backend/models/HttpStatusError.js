class HttpStatusError extends Error {
    // statusCode;
    // message;
    // topic;
    // name;

    constructor(statusCode, name, message, topic) {
        super();
        this._statusCode = statusCode;
        this._name = name;
        this._message = message;
        this._topic = topic;
    }

    get statusCode() { return this._statusCode; }
    get message() { return this._message; }
    get topic() { return this._topic; }
    get name() { return this._name; }

}
module.exports = HttpStatusError;