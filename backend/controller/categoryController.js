'use strict';

var mongoose = require('mongoose'),
    Category = mongoose.model('Category'),
    Note = mongoose.model('Note');
var ObjectId = require('mongoose').Types.ObjectId;
var jwt = require('jsonwebtoken');
var config = require('../config');


exports.list_all_categories = function (req, res) {
    let decoded = req.tokenDecoded;
    let userId = decoded.userId;
    console.log("list_all_categories");
    Category.find({
        userId: userId
    }, null, {sort: {name: -1}}, function (err, task) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(task);
        }
    });
};

exports.list_all_categories_with_notes = async function(req, res) {
    console.log("tokenDecoded", req.tokenDecoded.userId)
    let decoded = req.tokenDecoded;
    let userId = decoded.userId;
    console.log("list_all_categories_with_notes");
    let result = await Note.find({}).populate('categoryId');
    let userCategories = result.filter(item => item.categoryId && item.categoryId.userId == userId? item : null);

    let allUserCategories = await Category.find({userId: userId});
    console.log("allUserCategories.length", allUserCategories.length)

    let map = new Map();
    userCategories.forEach(note => {
        let category = note.categoryId;
        let notes = map.get(category);
        if (!notes) {
            notes = [];
        }
        // set category as id in note
        note.categoryId = category._id;
        note.category = category;
        notes.push(note);
        map.set(category, notes);
    });
    let categories = [];
    let categoryIds = [];
    for (var [key, value] of map.entries()) {
        let currentCategory = JSON.parse(JSON.stringify(key));
        Object.assign(currentCategory, {notes: value});
        categories.push(currentCategory);
        categoryIds.push(currentCategory._id)
      }
      let missingCats = allUserCategories.filter(item =>  categoryIds.indexOf(item._id + '') < 0 ? item : null );
      categories = categories.concat(missingCats)
      categories = categories.sort((a, b) => {
          if (a.color) {
              let compareColor = a.color.localeCompare(b.color);
              if (compareColor != 0) {
                  return compareColor;
              } else {
                  return a.name.localeCompare(b.name);
              }
          } else {
              return b;
          }
    }
      );
    res.json(categories);
}

exports.createACategory = function (req, res) {
    console.log("Create new category ");
    var category = new Category(req.body);
    category.userId = ObjectId(req.tokenDecoded.userId);
    category.save(function (err, category) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(category);
        }
    });
}

exports.getCategory = function (req, res) {
    console.log("getCategory");
    Category.findById(req.params.categoryId, function (err, task) {
        if (err) {
            res.status(500).send(err);
        } else {
            if (task && task.userId != req.tokenDecoded.userId){
                res.status(401).send();
            } else {
                res.json(task);
            }
        }
    });
};

exports.updateACategory = function (req, res) {
    console.log("Update category with id: ", req.params.categoryId);
    Category.findOneAndUpdate({
        _id: req.params.categoryId,
        userId: req.tokenDecoded.userId
    }, req.body, {
        new: true
    }, function (err, category) {
        console.log("updated, err: ", err);
        if (err) {
            res.status(500).send(err);
        } else {
            res.json(category);
        }
    });
};

exports.deleteCategory = function (req, res) {
    let categoryId = req.params.categoryId;
    Note.remove({
        categoryId: ObjectId(categoryId),
        userId: req.tokenDecoded.userId
    }, (err, result) => {
        if (!err)
            Note.historyModel().deleteMany({
                "d.categoryId": ObjectId(categoryId),
                "d.userId" : ObjectId(req.tokenDecoded.userId)
            }, (err, result) => {
                if (!err) {
                    Category.remove({
                        _id: categoryId
                    }, function (err, Category) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            exports.list_all_categories(req, res);
                        }
                    });
                }
            });
    });
};