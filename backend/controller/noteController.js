'use strict';

var mongoose = require('mongoose'),
    Category = mongoose.model('Category'),
    Note = mongoose.model('Note');
var ObjectId = require('mongoose').Types.ObjectId;

var validateUserAuthority = async function (req, res, categoryId) {
    let userId = req.tokenDecoded.userId;
    let result = await Category.findById(categoryId);
    if (!result || result.userId != userId) {
        return false;
    }
    return true;
}

var respondForInvalidAuthority = async function (req, res, categoryId) {
    let checkOkay = await validateUserAuthority(req, res, categoryId);
    if (checkOkay) {
        return true;
    } else {
        res.status(401).send();
    }
}

exports.getAllNotes = async function (req, res) {
    console.log("getAllNotes");
    var categoryId = req.params.categoryId;

    if (await respondForInvalidAuthority(req, res, categoryId)) {
        try {
            let notes = await Note.find({
                categoryId: new ObjectId(categoryId)
            });
            res.json(notes);
        } catch (error) {
            res.status(500).send(error);
        }
    }
};

exports.getNote = async function (req, res) {
    console.log("getNote");
    var noteId = req.params.noteId;
    var trashed = req.query.trashed === 'true';
    if (trashed) {
        console.log("return trashed one")
        Note.historyModel().find({
            "d._id": ObjectId(noteId)
        }, async (err, result) => {
            if (err) {
                res.status(500).send(error);
                return;
            }
            if (await respondForInvalidAuthority(req, res, result[0].d.categoryId)) {
                try {
                    res.json(result[0]['d']);
                } catch (error) {
                    res.status(500).send(error);
                }
            }
        });
    } else {
        Note.findById(noteId, async function (err, note) {
            if (err) {
                res.status(500).send(err);
            } else {
                console.log("Found note is: ", note);
                if (!note) {
                    res.status(404).send();
                } else if (await validateUserAuthority(req, res, note.categoryId)) {
                    res.json(note);
                } else {
                    res.status(401).send();
                }
            }
        });  
    }
}

exports.deleteNote = function (req, res) {
    console.log("deleteNote");
    Note.findById(req.params.noteId, async function (err, note) {
        if (err) {
            res.status(500).send(err);
        } else {
            if (await validateUserAuthority(req, res, note.categoryId)) {
                note.remove();
                res.end();
            } else {
                res.status(401).send();
            }
        }
    });
}

exports.createOrUpdateNote = async function (req, res) {
    console.log("createOrUpdateNote");
    var categoryId = req.params.categoryId;
    if (await respondForInvalidAuthority(req, res, categoryId)) {
        var noteId = req.body._id;
        var note = {
            name: req.body.name,
            content: req.body.content,
            type: req.body.type,
            markdown: req.body.markdown
        };
        note.categoryId = new ObjectId(categoryId);
        if (noteId) {
            Note.findByIdAndUpdate(noteId, note, {
                    new: true
                },
                (err, note) => {
                    if (err) {
                        res.status(500).send(err);
                    } else if (note != null) {
                        /* save for history */
                        note.save();
                        req.params.noteId = note._id;
                        exports.getNote(req, res);
                    } else {
                        res.status(404).send("No note with given id found");
                    }
                })
        } else {
            var note = new Note(note);
            note.save(function (err, note) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    req.params.noteId = note._id;
                    exports.getNote(req, res);
                }
            });
        }
    }
}

exports.getTrash = async function (req, res) {
    console.log("getTrash");
    Note.historyModel().find({
        o: "r",
    }, 'd content', async function (err, historyNotes) {
        if (err) {
            res.status(500).send(err);
        } else {
            var notes = [];
            for (const note of historyNotes) {
                if (await validateUserAuthority(req, res, note['d'].categoryId)) {
                    notes.push(note['d']);
                }
            }
            res.json(notes);
        }
    })
}
exports.deleteTrash = async function (req, res) {
    console.log("Delete Trash");

    Note.historyModel().aggregate([{
            $lookup: {
                from: "category",
                localField: "d.categoryId",
                foreignField: "_id",
                as: "category",
            }
        },
        {
            $match: {
                "category.userId": ObjectId(req.tokenDecoded.userId)
            }
        }
    ]).then(async (result) => {
        let onlyIds = result.map(obj => obj._id);
        try {
            let result =  await Note.historyModel().deleteMany({
                _id: {
                    $in: onlyIds
                }
            });
            res.send(result);
        } catch (error) {
            res.status(500).send(error);
        }
    });
}
exports.deleteNoteInTrash = async function (req, res) {
    console.log("DeleteNoteInTrash");
    var noteId = req.params.noteId;
    Note.historyModel().find({
        "d._id": ObjectId(noteId)
    }, async (err, result) => {
        if (err) {
            res.status(500).send(error);
            return;
        }
        if (await respondForInvalidAuthority(req, res, result[0].d.categoryId)) {
            try {
                let result = await Note.historyModel().deleteMany({
                "d._id": ObjectId(noteId)});
                res.send(result);
            } catch (error) {
                res.status(500).send(error);
            }
        }
    });
}

exports.getHistoryOfNote = function (req, res) {
    console.log("getHistoryOfNote");
    var noteId = req.params.noteId;
    Note.historyModel().find({"d._id": ObjectId(noteId), $or: [{"o": "u"}, {"o": "i"}]}, 't d content').sort({t:-1}).exec((err, result) => {
        if (err) {
            res.status(500).send(err);
        } else {
            var notes = [];
            result = result.sort((a,b) => {return a.t > b.t ? b : a});
            result.sort((a,b) => {return a.t > b.t ? a : b}).forEach(note => {
                let obj = note['d'];
                obj['date'] = note['t'];
                obj['historyId'] = note['_id'];
                notes.push(obj);
            });
            res.json(notes);    
        }
    });
}

exports.deleteHistoryEntryOfNote = function (req, res) {
    console.log("delete history entry");
    let noteId = req.params.noteId;
    let historyEntry = req.params.historyId;

    Note.historyModel().deleteOne({"_id": ObjectId(historyEntry)}).exec((err, result) => {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).send();
        }
    });
}

exports.restoreHistoryEntryOfNote = async function (req, res) {
    console.log("restore history entry");
    let noteId = req.params.noteId;
    let historyEntry = req.params.historyId;

    Note.historyModel().find({"_id": ObjectId(historyEntry)}, 'd content').exec((err, result) => {
        Note.findByIdAndUpdate(noteId, result[0]['d'], {
            new: true
        },
        async (err, note) => {
            if (err) {
                res.status(500).send(err);
            } else if (note != null) {
                /* save for history */
                await note.save();
                req.params.noteId = note._id;
                exports.getHistoryOfNote(req, res);
            } else {
                res.status(404).send("No note with given id found");
            }
        })
    });
}