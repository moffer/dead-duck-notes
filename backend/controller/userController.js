'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var config = require('../config');
var HttpStatusError = require('../models/HttpStatusError');
var authorization = require('./Authorization');

const saltRounds = 10;

exports.createUser = async function (req, res) {
    console.log("createUser");
    try {
        let username = req.body.username;
        if (!checkUsernameOkay(username)) {
            throw new HttpStatusError(406, "UserDoesNotMatch", "Username does not match conventions (only alphanumeric).", "User does not match.");
        }

        let password = req.body.password;
        /* Check for already exisiting user */
        let oldUser = await User.findOne({
            name: username
        });
        if (oldUser) {
            throw new HttpStatusError(406, "UserAlreadyExists", "Username is already in use.", "User already exists.");
        }
        
        let passwordHash = await getHashForPassword(password);
        var user = new User({
            name: username,
            passwordHash: passwordHash,
        });
        /* save the user */
        user.save(function (err) {
            if (err) throw err;
            
            console.log('User saved successfully');
            res.json({
                success: true,
                user: user.name
            });
        });
    } catch (error) {
        if (error instanceof HttpStatusError) {
            res.status(error.statusCode).json({
                name: error.name,
                topic: error.topic,
                message: error.message
            });
        } else {
            console.log(error);
        }
    }
};

exports.authenticate = async function (req, res) {
    console.log("authenticate")
    try {
        let password = req.body.password;
        var name = req.body.username;

        let user = await User.findOne({
            name: name
        });
        if (user == null) {
            throw new HttpStatusError(404, "UserNotFound", "User not found.", "User not found.");
        }
        var isCorrectPassword = await checkUser(user, password);
        if (isCorrectPassword) {
            var refreshToken = createRefreshToken();
            const payload = {
                admin: user.admin,
                name: user.name,
                userId: user._id,
                refreshToken: refreshToken
            };
            var token = createAccessToken(payload);
            var result = {
                success: true,
                message: 'Enjoy your token!',
                token: token
            };
            res.json(result);
        } else {
            throw new HttpStatusError(401, "AuthentificationFailed", "Authentification failed.", "Authentification failed.");
        }
    } catch (error) {
        if (error instanceof HttpStatusError) {
            res.status(error.statusCode).json({
                name: error.name,
                topic: error.topic,
                message: error.message
            });
        } else {
            console.log(error);
        }
    }
}

exports.refreshToken = function (req, res, next) {
    let token = authorization.getToken(req);
    try {
        jwt.verify(token, config.secret, {issuer: "notes-backend", ignoreExpiration: true});
    } catch (err) {
        return res.status(401).json({
            success: false,
            message: "Access token not valid."
        });
    }

    try {
        const payload = jwt.decode(token);
        delete payload.iat;
        delete payload.exp;
        delete payload.nbf;
        delete payload.jti;
        delete payload.iss;
        jwt.verify(payload.refreshToken, config.secret, {issuer: "notes-backend"});
        var refreshToken = createRefreshToken();
        payload.refreshToken = refreshToken;
        var newToken = createAccessToken(payload);
        return res.status(200).json({
            token: newToken,
            success: true,
            message: 'Renewed token.'
        });
    } catch (err) {
        console.log(err);
        return res.status(401).json({
            success: false,
            message: "Request token not valid."
        });
    }

}

var createRefreshToken = function() {
    return jwt.sign({}, config.secret, {
        expiresIn: 60 * 60 * 24 * 7, // s:m:s:d (--> 7 days)
        issuer: "notes-backend"
    });
}

var createAccessToken = function(payload) {
    return jwt.sign(payload, config.secret, {
        expiresIn: 24 * 60 * 60, // expires in 24 hours
        issuer: "notes-backend"
    })
}

var checkUsernameOkay = function (username) {
    return username && username.match(/^[A-Za-z0-9]*$/) != null;
}

var getHashForPassword = async function (plainPassword) {
    var hashedPassword = await bcrypt.hash(plainPassword, saltRounds);
    return hashedPassword;
}

var checkUser = async function (user, password) {
    const match = await bcrypt.compare(password, user.passwordHash);
    return match;
}