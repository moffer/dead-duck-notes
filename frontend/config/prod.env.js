'use strict'
const config = require('./config');

module.exports = {
  NODE_ENV: '"production"',
  dburl:  `'${config.basedburl}'`
}
