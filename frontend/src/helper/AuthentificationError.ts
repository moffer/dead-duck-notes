export default class AuthetificationError extends Error {
    constructor(message) {
        super(message);
        this.name = "AuthetificationError";
        Error.captureStackTrace(this, AuthetificationError);
      }
}