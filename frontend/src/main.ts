// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { VueMaterial } from 'vue-material'
import { UiAlert, UiButton } from 'keen-ui';
import KeenUI from 'keen-ui';
import 'keen-ui/dist/keen-ui.css';
import 'vuetify/dist/vuetify.min.css'
import { store } from './store';


Vue.config.productionTip = false

Vue.use(KeenUI);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
