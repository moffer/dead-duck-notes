import { EventBus } from "../helper/event-bus.js";
import axios, { AxiosResponse } from "axios";
import StorageService from "./StorageService";
import jwt_decode from 'jwt-decode';
import Services from './Services';
import UserService from "./UserService";
import AuthenticationError from '../helper/AuthentificationError';

export default class AuthenticatedService extends Services {

    get authenticatedAxios() {
        var INTER = axios.interceptors.request.use(
           async config => {
               // TODO: Rework with e.g. https://github.com/axios/axios/issues/266
                if (config.url.includes("refreshToken") || config.url.endsWith("authenticate") || config.url.endsWith("user")) {
                    return config;
                }
                let isValid :boolean = await new UserService().checkAndRenewTokens();
                if (!isValid) {
                    throw new AuthenticationError("Authentification failed");
                }
                
                config.headers.Authorization = this.createAuthBearer(StorageService.instance.jwt);
                return config;
            },
            function(error){return Promise.reject(error);}
          );
        let baseUrl:string = `${process.env.dburl}/authenticated`;
        let get = (url :string) => {
            return axios.get(baseUrl + url, this.createHeader(StorageService.instance.jwt));
        }
        let put = (url: string, data) => {
            return axios.put(baseUrl + url, data, this.createHeader(StorageService.instance.jwt));
        }
        let deleteFkt = (url: string) => {
            return axios.delete(baseUrl + url, this.createHeader(StorageService.instance.jwt));
        }
        
        return { get: get, put: put, delete: deleteFkt};
    }

    createHeader(token: string)  {
        return { headers: { 'Authorization': this.createAuthBearer(token) } };
    }

    createAuthBearer(token: string): string {
        return `Bearer ${token}`;
    }
}