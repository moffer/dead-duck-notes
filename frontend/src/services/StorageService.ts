export default class StorageService {
    private static _instance: StorageService;
    private _token: string;
    private _refreshToken: string;

    private constructor() { }

    static get instance() {
        if (!this._instance) {
            this._instance = new StorageService();
        }
        return this._instance;
    }

    get jwt(): string {
        return this._token || localStorage.getItem('token');
    }
    
    set jwt(token: string) {
        localStorage.setItem('token', token);
        this._token = token;
    }

    get refreshToken(): string {
        return this._refreshToken || localStorage.getItem('refreshToken');
    }
    
    set refreshToken(token: string) {
        localStorage.setItem('refreshToken', token);
        this._refreshToken = token;
    }

    invalidateToken() {
        this.jwt = "invalid";
        this._token = null;
    }
}