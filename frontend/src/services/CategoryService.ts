import axios, { AxiosResponse } from "axios";
import { EventBus } from "../helper/event-bus.js";
import { ICategory } from "../interfaces/ICategory.js";
import AuthenticatedService from "./AuthenticatedService";
import { INote } from "../interfaces/INote.js";

export default class CategoryService extends AuthenticatedService {

    loadCategories() {
        EventBus.$emit("loading", true);
        let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
            this.authenticatedAxios.get("/category").then(
                result => {
                    resolve(result);
                    EventBus.$emit("loading", false);
                },
                error => {
                    this.onError(error);
                    reject(error);
                }
            )
        });
        return promise;
    }

    async loadCategoriesWithNotes() {
        EventBus.$emit("loading", true);
        let result: AxiosResponse<ICategory[]>;
        try {
            result = await this.authenticatedAxios.get('/category/categoryWithNotes');
        } catch (error) {
            this.onError(error);
        } finally {
            EventBus.$emit("loading", false);
        }
        return result;
    }

    loadCategory(categoryId: string) {
        EventBus.$emit("loading", true);
        let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
            this.authenticatedAxios.get(`/category/${categoryId}`).then(
                result => {
                    resolve(result);
                    EventBus.$emit("loading", false);
                },
                error => {
                    this.onError(error);
                    reject(error);
                }
            );
        });
        return promise;
    }
    saveCategory(categoryId: string, category: ICategory) {
        let url = `/category/${categoryId ? categoryId : ''}`;
        let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
            this.authenticatedAxios.put(url, category).then(
                result => {
                    resolve(result);
                    EventBus.$emit("loading", false);
                },
                error => {
                    this.onError(error);
                    reject(error);
                }
            )
        });
        return promise;
    }

    deleteCategory(categoryId: string) {
        let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
            EventBus.$emit("loading", true);
            this.authenticatedAxios.delete(`/category/${categoryId}`).then(
                result => {
                    resolve(result);
                    EventBus.$emit("loading", false);
                },
                error => {
                    this.onError(error);
                    reject(error);
                }
            )
        });
        return promise;
    }
}
