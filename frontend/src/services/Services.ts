import { EventBus } from "../helper/event-bus.js";
import axios, { AxiosResponse } from "axios";
import StorageService from "./StorageService";
import jwt_decode from 'jwt-decode';
import { debug } from "util";
import AuthetificationError from "../helper/AuthentificationError";

export default class Service {

    get translate() {
        return {
            noNetwork: "Verbindung fehlgeschlagen. Bitte nochmal versuchen.",
            ValidationError: "Daten sind nicht gültig oder nicht vollständig.",
            UserAlreadyExists: "Der Username existiert bereits.",
            UserDoesNotMatch: "Der Username darf nur aus alphanumerischen Zeichen bestehen (Alphabet, 0-9).",
            UserNotFound: "Der Username existiert nicht. Bitte registrieren",
            AuthentificationFailed: "Username und Passwort stimmen nicht überein.",
            AuthentificationTimedOut: "Die Session ist abgelaufen. Bitte erneut einloggen."
        };
    }

    onError(error) {
        let message: string = error.message;
        if (error.name == "AuthetificationError") {
            EventBus.$emit("authetification", false);
            message = this.translate.AuthentificationTimedOut;
        } else if (error.message == "Network Error") {
            message = this.translate.noNetwork;
        } else if (error.response && error.response.data) {
            if (this.translate[error.response.data.name]) {
                message = this.translate[error.response.data.name];
            } else {
                message = error.response.data.message;
            }
        } else {
            console.error(error);
        }
        EventBus.$emit("message", { message });
        EventBus.$emit("loading", false);
    }
}