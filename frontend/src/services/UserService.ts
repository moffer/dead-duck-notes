import axios, { AxiosResponse } from "axios";
import { EventBus } from "../helper/event-bus.js";
import Services from "./Services";
import jwt_decode from 'jwt-decode';
import { debug } from "util";
import StorageService from "./StorageService";


export default class UserService extends Services {

    async registerUser(username: string, password: string) {
        EventBus.$emit("loading", true);
        let user = { username: username, password: password };
        let result: AxiosResponse<any>;
        try {
            result = await axios.post(`${process.env.dburl}/user`, user);
        } catch (error) {
            this.onError(error);
        } finally {
            EventBus.$emit("loading", false);
        }
        return result;
    }

    async signIn(username: string, password: string) {
        EventBus.$emit("loading", true);
        let user = { username: username, password: password };
        let result: AxiosResponse<any>;
        try {
            result = await axios.post(`${process.env.dburl}/authenticate`, user);
        } catch (error) {
            this.onError(error);
        } finally {
            EventBus.$emit("loading", false);
        }
        return result;
    }



    async checkAndRenewTokens() {
        if (!this.isTokenValid(StorageService.instance.jwt)) {
            try {
                let result = await this.refreshToken(StorageService.instance.jwt);
                if (result.data.success) {
                    let newToken = result.data.token;
                    StorageService.instance.jwt = newToken;
                    return true;
                } else  {
                    return false;
                }
            } catch (error) {
                return false;
            }
        }
        return true;
    }

    isTokenValid(token: string): boolean {
        if (!token || token == "invalid") return false;

        var decoded = jwt_decode(token);
        const exp = decoded.exp;

        if (exp - (Date.now() / 1000) < 0) {
            return false;
        } else {
            return true;
        }
    }

    
    isRefreshTokenValid(token: string): boolean {
        if (!token) return false;

        var decoded = jwt_decode(token);
        var refreshToken = decoded.refreshToken;
        return this.isTokenValid(refreshToken);
    }

    async refreshToken(token: string) {
        EventBus.$emit("loading", true);
        let result: AxiosResponse<any>;
        try {
            result = await axios.post(`${process.env.dburl}/refreshToken`, { }, { headers: { 'Authorization': `Bearer ${token}` } });
        } catch (error) {
            this.onError(error);
            throw error;
        } finally {
            EventBus.$emit("loading", false);
        }
        return result;
    }
}