import { AxiosResponse } from "axios";
import { EventBus } from "../helper/event-bus.js";
import { INote } from "../interfaces/INote.js";
import AuthenticatedService from "./AuthenticatedService";

export default class NoteService extends AuthenticatedService {


    deleteNoteFromTrash(noteId: string) {
        EventBus.$emit("loading", true);
        let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
            this.authenticatedAxios.delete(`/trash/${noteId}`).then(
                result => {
                    resolve(result);
                    EventBus.$emit("loading", false);
                },
                error => {
                    this.onError(error);
                    reject(error);
                }
            );
        });
        return promise;
    }

    deleteAllNotesFromTrash() {
        console.log("delete all notes");
        EventBus.$emit("loading", true);
        let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
            this.authenticatedAxios.delete(`/trash`).then(
                result => {
                    resolve(result);
                    EventBus.$emit("loading", false);
                },
                error => {
                    this.onError(error);
                    reject(error);
                }
            )
        });
        return promise;
    }

    deleteNote(categoryId: string, noteId: string, trashed: boolean) {
        if (trashed) {
            let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
                EventBus.$emit("loading", true);
                this.authenticatedAxios.delete(`/trash/${noteId}`).then(
                    result => {
                        resolve(result);
                        EventBus.$emit("loading", false);
                    },
                    error => {
                        this.onError(error);
                        reject(error);
                    }
                )
            })
            return promise;
        } else {
            let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
                EventBus.$emit("loading", true);
                this.authenticatedAxios.delete(`/category/${categoryId}/note/${noteId}`).then(
                    result => {
                        resolve(result);
                        EventBus.$emit("loading", false);
                    },
                    error => {
                        this.onError(error);
                        reject(error);
                    }
                )
            })
            return promise;
        }
    }

    getTrash() {
        console.log("Trash");
        let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
            EventBus.$emit("loading", true);
            this.authenticatedAxios.get(`/trash`).then(
                result => {
                    resolve(result);
                    EventBus.$emit("loading", false);
                },
                error => {
                    this.onError(error);
                    reject(error);
                }
            )
        });
        return promise;
    }

    loadNotes(categoryId: string) {
        EventBus.$emit("loading", true);
        let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
            this.authenticatedAxios.get(`/category/${categoryId}/note`).then(
                result => {
                    resolve(result);
                    EventBus.$emit("loading", false);
                },
                error => {
                    this.onError(error);
                    reject(error);
                }
            );
        });
        return promise;
    }

    loadNote(categoryId: string, noteId: string, trashed: boolean) {
        EventBus.$emit("loading", true);
        let promise = new Promise<AxiosResponse<any>>((resolve, reject) => {
            this.authenticatedAxios.get(`/category/${categoryId}/note/${noteId}?trashed=${trashed}`
            ).then(
                result => {
                    resolve(result);
                    EventBus.$emit("loading", false);
                },
                error => {
                    this.onError(error);
                    reject(error);
                }
            );
        });
        return promise;
    }

    async saveNote(categoryId: string, note: INote) {
        EventBus.$emit("loading", true);
        try {
            const result = await this.authenticatedAxios.put(`/category/${categoryId}/note`, note);
            EventBus.$emit("loading", false);
            return result;
        } catch (error) {
            this.onError(error);
            throw error;
        }
    }

    async getHistoryOfNote(categoryId: string, noteId: string) {
        EventBus.$emit("loading", true);
        try {
            const result = await this.authenticatedAxios.get(`/category/${categoryId}/note/${noteId}/history`);
            EventBus.$emit("loading", false);
            return result;
        } catch (error) {
            this.onError(error);
            throw error;
        }
    }

    async deleteNoteHistoryEntry(categoryId: any, noteId: any, historyId: any) {
        EventBus.$emit("loading", true);
        try {
            const result = await this.authenticatedAxios.delete(`/category/${categoryId}/note/${noteId}/history/${historyId}`);
            EventBus.$emit("loading", false);
            return result;
        } catch (error) {
            this.onError(error);
            throw error;
        }
    }
    async restoreNoteHistoryEntry(categoryId: any, noteId: any, historyId: any) {
        const data = { categoryId, noteId, historyId };
        EventBus.$emit("loading", true);
        try {
            const result = await this.authenticatedAxios.put(`/category/${categoryId}/note/${noteId}/history/${historyId}/restore`, data);
            EventBus.$emit("loading", false);
            return result;
        } catch (error) {
            this.onError(error);
            throw error;
        }
    }
}
