import { MutationTree, GetterTree, ActionTree, Module } from "vuex";
import { IUser } from "../../interfaces/IUser";
import UserService from "../../services/UserService";
import StorageService from "../../services/StorageService";
import router from "../../router";
import { StateNamespaces } from "../namespaces";
import { CategoryActionTypes } from "../category-state/category-state";

///////////////////////////////////////
// State
///////////////////////////////////////
export interface UserState {
  user: IUser;
}

///////////////////////////////////////
// Mutation
///////////////////////////////////////
export enum UserMutationTypes {
  SET_USER = "setUserMutation",
  RESET_USER = "resetUserMutation"
}

export const mutations: MutationTree<UserState> = {
  [UserMutationTypes.SET_USER]: (state: UserState, payload: IUser) => {
    state.user = payload;
  },
  [UserMutationTypes.RESET_USER]: (state: UserState) => {
    state.user = undefined;
  }
};

///////////////////////////////////////
// Actions
///////////////////////////////////////
export type SetUserAction = (payload: IUser) => void;
export type LogoutUserAction = () => void;
export type LoginUserAction = (payload) => void;

export enum UserActionTypes {
  SET_USER = "setUserAction",
  LOGOUT = "logoutUserAction",
  LOGIN = "loginUserAction"
}

const categoryState = StateNamespaces.CATEGORY_STATE;

export const actions: ActionTree<UserState, any> = {
  [UserActionTypes.SET_USER](context, payload: IUser) {
    context.commit(UserMutationTypes.SET_USER, payload);
  },
  [UserActionTypes.LOGOUT](context) {
    StorageService.instance.invalidateToken();
    context.commit(UserMutationTypes.RESET_USER);
    router.push({ name: "SignIn" });
  },
  [UserActionTypes.LOGIN](context, payload: any) {
    new UserService()
      .signIn(payload.username, payload.password)
      .then(result => {
        if (result && result.status == 200) {
          StorageService.instance.jwt = result.data.token;
          router.push({ name: "Layout" });
          context.commit(UserMutationTypes.SET_USER, result.data);
          // Daten laden
          context.dispatch(
            categoryState + "/" + CategoryActionTypes.UPDATE_CATEGORY_LIST,
            {},
            { root: true }
          );
        }
      });
  }
};

///////////////////////////////////////
// Getters
///////////////////////////////////////
export const getters: GetterTree<UserState, any> = {
  user: (state): IUser => {
    return state.user;
  }
};

///////////////////////////////////////
// Module
///////////////////////////////////////
const initialState: UserState = {
  user: undefined
};

export const userModule: Module<UserState, any> = {
  namespaced: true,
  state: initialState,
  getters,
  mutations,
  actions
};
