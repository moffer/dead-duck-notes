import Vue from "vue";
import Vuex from "vuex";
import { StateNamespaces } from "./namespaces";
import * as categoryState from "./category-state/category-state";
import * as userState from "./user-state/user-state";

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    [StateNamespaces.CATEGORY_STATE]: categoryState.categoryModule,
    [StateNamespaces.USER_STATE]: userState.userModule
  }
});
