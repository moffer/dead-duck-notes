export enum StateNamespaces {
  CATEGORY_STATE = "CategoryModule",
  USER_STATE = "UserModule"
}
