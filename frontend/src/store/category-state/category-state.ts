import { Category } from "../../interfaces/gui-interfaces/Category";
import { MutationTree, GetterTree, ActionTree, Module } from "vuex";
import { Note } from "../../interfaces/gui-interfaces/Note";
import CategoryService from "../../services/CategoryService";
import { ICategory } from "../../interfaces/ICategory";
import NoteService from "../../services/NoteService";
import { INote } from "../../interfaces/INote";
import { EventBus } from "../../helper/event-bus.js";
import router from "@/router/index";
import Vue from "vue";
import { AxiosResponse } from "axios";
///////////////////////////////////////
// State
///////////////////////////////////////
export interface CategoryState {
  historyOfNote: any;
  allCategories: Category[];
  trashedNotes: Note[];
}

///////////////////////////////////////
// Mutation
///////////////////////////////////////
export enum CategoryMutationTypes {
  SET_CATEGORIES = "setCategoriesMutation",
  ADD_CATEGORY = "addCategoryMutation",
  CHANGE_NAME = "changeCategoryNameMutation",
  ADD_NOTE = "addNoteMutation",
  UPDATE_NOTE = "updateNoteMutation",
  SET_TRASH = "setTrashMutation",
  REMOVE_NOTE = "removeNoteMutation",
  REMOVE_NOTE_FROM_TRASH = "removeNoteFromTrashMutation",
  REMOVE_CATEGORY = "removeCategoryMutation",
  SET_HISTORY = "SET_HISTORY",
  REMOVE_FROM_HISTORY = "REMOVE_FROM_HISTORY"
}

export const mutations: MutationTree<CategoryState> = {
  [CategoryMutationTypes.SET_CATEGORIES]: (
    state: CategoryState,
    payload: Category[]
  ) => {
    state.allCategories = payload;
  },
  [CategoryMutationTypes.ADD_CATEGORY]: (
    state: CategoryState,
    payload: Category
  ) => {
    if (!state.allCategories) state.allCategories = [];

    state.allCategories.push(payload);
  },
  [CategoryMutationTypes.REMOVE_CATEGORY]: (
    state: CategoryState,
    payload: Category
  ) => {
    if (state.allCategories) {
      var index = state.allCategories
        .map(el => {
          return el._id;
        })
        .indexOf(payload._id);
      state.allCategories.splice(index, 1);
    }
  },
  [CategoryMutationTypes.CHANGE_NAME]: (
    state: CategoryState,
    payload: Category
  ) => {
    state.allCategories.forEach((element: Category) => {
      if (payload._id == element._id) {
        element.name = payload.name;
        element.color = payload.color;
      }
    });
  },
  [CategoryMutationTypes.ADD_NOTE]: (state: CategoryState, payload: Note) => {
    if (state.allCategories) {
      const category: Category = state.allCategories.find(
        (category: Category) => category._id == payload.categoryId
      );
      if (!category.notes) category.notes = [];
      category.notes.push(payload);
    }
  },
  [CategoryMutationTypes.UPDATE_NOTE]: (
    state: CategoryState,
    payload: Note
  ) => {
    if (state.allCategories) {
      const category: Category = state.allCategories.find(
        (category: Category) => category._id == payload.categoryId
      );
      let note: Note = category.notes.find(
        (note: Note) => note._id == payload._id
      );
      note.content = payload.content;
      note.markdown = payload.markdown;
      note.name = payload.name;
    }
  },
  [CategoryMutationTypes.SET_TRASH]: (state: CategoryState, trash: Note[]) => {
    state.trashedNotes = trash;
  },
  [CategoryMutationTypes.REMOVE_NOTE]: (
    state: CategoryState,
    payload: Note
  ) => {
    if (state.allCategories) {
      const category: Category = state.allCategories.find(
        (category: Category) => category._id == payload.categoryId
      );
      if (category.notes) {
        const index = category.notes.indexOf(payload);
        category.notes.splice(index, 1);
      }
    }
  },
  [CategoryMutationTypes.REMOVE_NOTE_FROM_TRASH]: (
    state: CategoryState,
    payload: Note
  ) => {
    if (state.trashedNotes) {
      const index = state.trashedNotes.indexOf(payload);
      state.trashedNotes.splice(index, 1);
    }
  },
  [CategoryMutationTypes.SET_HISTORY]: (state: CategoryState, payload: any) => {
    state.historyOfNote = payload;
  },
  [CategoryMutationTypes.REMOVE_FROM_HISTORY]: (state: CategoryState, payload: any) => {
    if (state.historyOfNote) {
      const index = state.historyOfNote.indexOf(payload);
      state.historyOfNote.splice(index, 1);
    }
  }

};

///////////////////////////////////////
// Actions
///////////////////////////////////////
export type SetCategoriesAction = (payload: Category[]) => void;
export type SaveCategoryAction = (payload: Category) => void;
export type UpdateCategoryListAction = () => void;
export type SaveNoteAction = (payload: Note) => void;
export type LoadTrashedNoteAction = () => void;
export type SetNoteAction = (payload: any) => void;
export type DeleteNoteAction = (payload: any) => void;
export type DeleteCategoryAction = (payload: Category) => void;
export type LoadHistoryAction = (payload: Note) => void;
export type DeleteNoteHistoryAction = (payload: any) => void;
export type RestoreNoteHistoryAction = (payload: any) => void;

export enum CategoryActionTypes {
  SET_CATEGORIES="setCategoriesAction",
  SAVE_CATEGORY="saveCategoryAction",
  UPDATE_CATEGORY_LIST="updateCategoryListAction",
  SAVE_NOTE="saveNoteAction",
  LOAD_TRASH_NOTES="loadTrashedNoteAction",
  DELETE_NOTE="deleteNoteAction",
  DELETE_CATEGORY="deleteCategoryAction",
  DELETE_NOTE_HISTORY="DELETE_NOTE_HISTORY",
  LOAD_HISTORY="LOAD_HISTORY",
  RESTORE_NOTE_HISTORY="RESTORE_NOTE_HISTORY"
}

export const actions: ActionTree<CategoryState, any> = {
  async [CategoryActionTypes.SAVE_CATEGORY](context, category: Category) {
    const isNew: boolean = category._id ? false : true;
    const result = await new CategoryService().saveCategory(
      category._id,
      category
    );
    if (result.status == 200) {
      if (!result.data.notes) result.data.notes = [];
      if (isNew) {
        context.commit(CategoryMutationTypes.ADD_CATEGORY, result.data);
      } else {
        context.commit(CategoryMutationTypes.CHANGE_NAME, result.data);
      }
    }
    return result;
  },
  async [CategoryActionTypes.DELETE_CATEGORY](context, category: Category) {
    const result = await new CategoryService().deleteCategory(category._id);
    if (result.status == 200) {
      context.commit(CategoryMutationTypes.REMOVE_CATEGORY, category);
    }
    return result;
  },
  async [CategoryActionTypes.LOAD_TRASH_NOTES](context) {
    let result: AxiosResponse<any> = await new NoteService().getTrash();
    if (result.status == 200) {
      context.commit(CategoryMutationTypes.SET_TRASH, result.data);
    }
    return result;
  },
  async [CategoryActionTypes.UPDATE_CATEGORY_LIST](context) {
    let result: AxiosResponse<
      any
    > = await new CategoryService().loadCategoriesWithNotes();
    if (result.status == 200) {
      result.data.forEach(element => {
        if (!element.notes) element.notes = [];
      });
      context.commit(CategoryMutationTypes.SET_CATEGORIES, result.data);
    }
    return result;
  },
  async [CategoryActionTypes.SAVE_NOTE](context, payload: INote) {
    let result: AxiosResponse<any> = await new NoteService().saveNote(
      payload.categoryId,
      payload
    );
    if (result.status == 200) {
      if (!payload._id) {
        context.commit(CategoryMutationTypes.ADD_NOTE, result.data);
      } else {
        context.commit(CategoryMutationTypes.UPDATE_NOTE, result.data);
      }

      EventBus.$emit("message", { message: "Notiz aktualisiert" });
    }
    return result;
  },
  async [CategoryActionTypes.DELETE_NOTE](context, payload: any) {
    const note = payload.note;
    const fromTrash = payload.fromTrash;
    let result = await new NoteService().deleteNote(
      note.categoryId,
      note._id,
      fromTrash
    );

    if (result.status == 200) {
      if (fromTrash) {
        router.push({ name: "Trash" });
        context.commit(CategoryMutationTypes.REMOVE_NOTE_FROM_TRASH, note);
      } else {
        context.commit(CategoryMutationTypes.REMOVE_NOTE, note);
        router.push({ name: "NoteListView1" });
      }
      // context.commit(CategoryMutationTypes.SET_CURRENT_NOTE, null);
    }
    return result;
  },
  async [CategoryActionTypes.LOAD_HISTORY](context, payload: Note) {
    const result: AxiosResponse<any> = await new NoteService().getHistoryOfNote(
      payload.categoryId,
      payload._id
    );
    context.commit(CategoryMutationTypes.SET_HISTORY, result.data);
  },  
  async [CategoryActionTypes.DELETE_NOTE_HISTORY](context, payload: any) {
    const historyId = payload.historyId;
    const noteId = payload._id;
    const categoryId = payload.categoryId;
    let result = await new NoteService().deleteNoteHistoryEntry(
      categoryId,
      noteId,
      historyId
    );

    if (result.status === 200) {
      context.commit(CategoryMutationTypes.REMOVE_FROM_HISTORY, payload);
    }
  },
  async [CategoryActionTypes.RESTORE_NOTE_HISTORY](context, payload: any) {
    const result = await new NoteService().restoreNoteHistoryEntry(
      payload.categoryId,
      payload._id,
      payload.historyId
    );

    if (result.status === 200) {
      context.commit(CategoryMutationTypes.SET_HISTORY, result.data);
      const note = result.data[0];
      context.commit(CategoryMutationTypes.UPDATE_NOTE, note);
    }

  }
};

///////////////////////////////////////
// Getters
///////////////////////////////////////
export const getters: GetterTree<CategoryState, any> = {
  categories: (state: CategoryState): Category[] => {
    return state.allCategories;
  },
  currentCategoryByRoute: (state: CategoryState) => (categoryId: string) => {
    if (state.allCategories) {
      return state.allCategories.find(
        (category: Category) => category._id == categoryId
      );
    }
    return null;
  },
  trashedNotes: (state: CategoryState): Note[] => {
    return state.trashedNotes;
  },
  currentNoteByRoute: (state: CategoryState, getters1) => (
    categoryId: string,
    noteId: string,
    isTrash: boolean
  ): Note => {
    if (!isTrash) {
      const currentCategory: Category = getters1.currentCategoryByRoute(
        categoryId
      );
      if (currentCategory) {
        return currentCategory.notes.find((note: Note) => note._id == noteId);
      }
    } else {
      if (state.trashedNotes) {
        return state.trashedNotes.find((note: Note) => note._id == noteId);
      }
    }
    return null;
  },
  historyOfNote: (state: CategoryState) => {
    return state.historyOfNote;
  }
};

///////////////////////////////////////
// Module
///////////////////////////////////////
const initialState: CategoryState = {
  allCategories: undefined,
  trashedNotes: undefined,
  historyOfNote: undefined
};

export const categoryModule: Module<CategoryState, any> = {
  namespaced: true,
  state: initialState,
  getters,
  mutations,
  actions
};
