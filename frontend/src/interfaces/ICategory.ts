import { INote } from "./INote";

export interface ICategory {
    _id?: string,
    name: string,
    color?: string,
    notes: INote[]
}
