import { ICategory } from "./ICategory";

export interface INote {
    _id?: string;
    type: NoteType;
    name: string;
    content?: INoteContent;
    categoryId: string;
    category: ICategory;
    markdown: boolean;
}
export enum NoteType {
    checklist = 'checklist',
    text = 'text'
}

export interface INoteContent {
    text?: string;
    noteItems?: INoteItem[];
}
export interface INoteItem {
    name: string;
    checked?: boolean;
}