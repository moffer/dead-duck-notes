import { INote, NoteType, INoteContent } from "../INote";
import { ICategory } from "../ICategory";

export class Note implements INote {
    _id?: string;
    type: NoteType;
    name: string;
    content?: INoteContent;
    categoryId: string;
    category: ICategory
    markdown: boolean;
}