import { ICategory } from "../ICategory";

export class Category implements ICategory {
    _id?: string;
    name: string;
    color: string;
    notes: any[];


    model: boolean;
}