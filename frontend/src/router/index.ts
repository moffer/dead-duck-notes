import Vue from 'vue'
import Router from 'vue-router'
import NoteListView1 from '@/views/NoteListView.vue'
import TrashView from '@/views/TrashView.vue'
import RegisterView from '@/views/RegisterView.vue'
import SignInView from '@/views/SignInView.vue'
import NoteContentView from '@/views/NoteContentView.vue'
import AppLayoutView from '@/views/AppLayoutView.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/layout', name: 'Layout', component: AppLayoutView,
      children: [
        {
          path: 'trash', name: 'Trash', component: TrashView,
        },
        {
          name: "TrashNoteContent",
          path: 'trash/:categoryid/:noteid', component: NoteContentView
        },
        {
          path: 'noteList/:categoryid',
          component: NoteListView1, name: "NoteListView1"
        },
        {
          path: 'noteContent/:categoryid/:noteid',
          component: NoteContentView, name: "LayoutNoteContent"
        }
      ]
    },
    {
      path: '/register',
      name: 'Register',
      component: RegisterView
    },

    {
      path: '/',
      name: 'SignIn',
      component: SignInView
    },

  ]
})
