# README #

### What is this repository for? ###

Note-Application in the style of ColorNote (Android Application) and Wunderlist. Advantage is, that you can run on your own server and database. Also the plan is to combine the two applications (Wunderlist does not support text-notes.

As creating the repository I listened to the song "Tote Enten" of Tim Toupet.

### How do I get set up? ###

# dead-duck-notes

> Note-application

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```
### Build with Windows
Run following npm command in administrator mode:
```npm install -g windows-build-tools```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact