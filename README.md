# About
Note-Application in the style of ColorNote (Android Application) and Wunderlist. But the advantage will be, that you can run on your own server and database. Also there is the idea to combine the two applications (Wunderlist does not support text-notes).

## Naming
While creating the repository I listened to the song "Tote Enten" of Tim Toupet.

# Setup
Application is inteded to be used with docker.  
First build frontendas backend with it: 

## Backend
Build and use backend with following steps:
- `docker build -f ./backend/docker/Dockerfile ./backend`
- Use backend/docker/docker-compose.yml, enter the image you build.
- Either use traefik or other Reverse proxy to access the backend.

## Frontend
Build and use frontend with following steps:
- `docker build -f ./frontend/docker/Dockerfile ./frontend/`
- Use again a Reverse proxy to make the frontend accessable.

# Development
For development of the backend/frontend you can use docker-compose and start the dev-compose file: `docker-compose -f docker-compose.dev.yml up`.